package dynamusic.controllers;

import org.springframework.web.bind.annotation.*;

@RestController
public class HomeRESTController {

    private static final String GREATING_TEMPLATE = "Hello, %s!";

    @GetMapping("/{user_name}")
    public String serviceRootRequest(@RequestParam(value = "user_name", defaultValue = "Guest") String name){
        return String.format(GREATING_TEMPLATE, name);
    }
}
